import numpy as np
from typing import Dict


class ProductModel(object):
    def __init__(self, product_id: int, features: [int]):
        self.product_id: int = product_id
        self.features: [int] = features


class RecommendationResult(object):
    def __init__(self, product_id: int, score: float, product_features: [int]):
        self.product_id: int = product_id
        self.score: float = score
        self.product_features: [int] = product_features


class RecSys(object):
    def __init__(self):
        self.products_dict: Dict[int, ProductModel] = {}

    def _validate_product_ids(self, product_ids: [int]) -> [int]:
        return [
            product_id
            for product_id in product_ids
            if product_id in self.products_dict
        ]

    """ STEP 1/4 (10min) """
    def add_product(self, product_id: int, features: [int]):
        """
        This method adds product ids and features for product recommendation feature.
        :param product_id: product id
        :param features: product features

        CODING GUIDELINE
        - Create product (ProductModel) instance;
        - Add product instance to product data dictionary (self.products_dict)
            using product id (product_id) as key.
        """
        pass

    """ STEP 2/4 (30min) """
    def create_user_profile(self, product_ids: [int]) -> [float]:
        """
        This method creates a user profile based on products of interest,
            allowing users to be compared to product features (product profiles)
        :param product_ids: Ids of user's products of interest
        :return: User profile

        CODING GUIDELINE
        - Sum all feature values of user's products, creating a new user feature vector;
        - Divide the new feature vector by the number of user's products;
        - Return the modified feature vector as the user profile.

        - Example:
          - Product features:
            - BC - Books, magazines & comics
            - FB - Food & beverages
            - GM - Games
            - HD - House & decoration
            - JW - Jewes & watches
            - MI - Musical instruments
            - MM - Music & movies
            - SC - Shoes & clothes
            - SF - Sporting & fitness
            - TH - Toys & hobbies
          - Feature vector (vector pattern):
            - [BC, FB, GM, HD, JW, MI, MM, SC, SF, TH]
          - Full product list:
            - Product #1: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
            - Product #2: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
            - Product #3: [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
            - Product #4: [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
          - User's products of interest (input parameter "products_ids")
            - [1, 2, 3]
          - User preferences - 3 products of interest:
            - Product #1: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
            - Product #2: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
            - Product #3: [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
          - Sum of feature vectors:
            - [0, 2, 1, 0, 0, 0, 0, 0, 0, 0]
          - Feature vectors divided by the number of products of interest:
            - [0/3, 2/3, 1/3, 0/3, 0/3, 0/3, 0/3, 0/3, 0/3, 0/3]
          - User profile:
            - [0, 0.67, 0.33, 0, 0, 0, 0, 0, 0, 0]

        NOTE: Numpy arrays (https://numpy.org/doc/stable/user/whatisnumpy.html)
        may be used as tool for feature vector operations.
        """
        pass

    """ STEP 3/4 (20min) """
    @staticmethod
    def _cosine_similarity(features_1: [float], features_2: [float]) -> float:
        """
        This method calculates cosine similarity using features as input data,
        allowing feature vectors to be compared to each other.
        :param features_1: First feature vector to be used as input for similarity computation;
        :param features_2: Second feature vector to be used as input for similarity computation;
        :return: Value indicating the similarity measure between the feature vectors.

        CODING GUIDELINE
        - Calculate similarity as:
          - inner(features_1, features_2) / ( norm(features_1) * norm(features_2) )
        - Return similarity result

        MORE INFO
        - https://en.wikipedia.org/wiki/Cosine_similarity (21/09/2021)

        NOTE: Numpy arrays (https://numpy.org/doc/stable/user/whatisnumpy.html)
        may be used as tool for feature vector operations.
        """
        pass

    """ STEP 4/4 (40min) """
    def get_recommendations(self, product_ids: [int]) -> [RecommendationResult]:
        """
        This method (1) calculates the user profile based on his products of interest (input parameter "product_ids"),
        (2) computes the (cosine) similarity between the user profile and each product's features
        and (3) returns the product list inversely ordered by similarity values.
        :param product_ids: User's products of interest ids
        :return: Recommendation results

        CODING GUIDELINE
        - Calculate the user profile based on his products of interest (input parameter "product_ids"),
            by calling method "create_user_profile";
        - Compute the (cosine) similarity between the user profile and each product features (product profile);
        - Return the product list inversely ordered by similarity values.

        NOTE: Method "_cosine_similarity" should be used to compute similarity
            measures for feature vectors comparisons.

        NOTE: This method should return a list of RecommendationResult
            instances as recommendation results

        WHEN APPLICATION IS FINISHED:
        - Default product list:
          [
            (1, [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]),
            (2, [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]),
            (3, [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]),
            (4, [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]),
            (5, [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]),
            (6, [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]),
            (7, [0, 0, 0, 0, 0, 1, 0, 0, 0, 0])
          ]
        - Run application: python main.py -o=gr -pids="[1, 2, 4]"
        """
        return []
