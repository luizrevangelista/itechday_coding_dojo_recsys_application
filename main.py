import argparse
import json
from typing import Tuple, List

from rec_sys import RecSys, RecommendationResult


class AppParameters(object):
    def __init__(
            self,
            option: str,
            product_id: int,
            product_features: [str],
            product_ids: [int],
    ):
        self.option: str = option
        self.product_id: int = product_id
        self.product_features: [str] = product_features
        self.product_ids: [int] = product_ids


class App(object):
    def __init__(self, product_data: List[Tuple[int, List[int]]] = None):
        self.parameters: AppParameters = self._read_parameters()
        self.rec_sys: RecSys = RecSys()
        if product_data:
            self._init_products(product_data=product_data)

    @staticmethod
    def _read_parameters() -> AppParameters:
        ap = argparse.ArgumentParser()
        ap.add_argument(
            '-o', '--option',
            type=str,
            help='Option: "ap" (Add Product), "gr" (Get recommendations)'
        )
        ap.add_argument(
            '-pid', '--product_id',
            type=str,
            default=None,
            help='Product id for product indexing operation')
        ap.add_argument(
            '-pf', '--product_features',
            type=str,
            default=None,
            help='Product features for product indexing operation')
        ap.add_argument(
            '-pids', '--product_ids',
            type=str,
            default=None,
            help='Product ids for user recommendations')

        args = vars(ap.parse_args())
        product_features = json.loads(args['product_features']) if args['product_features'] else None
        product_ids = json.loads(args['product_ids']) if args['product_ids'] else None

        return AppParameters(
            option=args['option'],
            product_id=args['product_id'],
            product_features=product_features,
            product_ids=product_ids
        )

    def _init_products(self, product_data: List[Tuple[int, List[int]]]):
        for product_info in product_data:
            self.rec_sys.add_product(
                product_id=product_info[0],
                features=product_info[1]
            )

    @staticmethod
    def _show_recommendation_results(
            user_profile: [int],
            recommendation_results: [RecommendationResult]
    ):
        print('\n------')
        print('SIMPLE RECOMMENDATION APPLICATION')
        print('--')
        print('- USER PROFILE:', user_profile)
        print('- RECOMMENDATION RESULT(S):')
        if not recommendation_results:
            print('    - No results found.')
        for result in recommendation_results or []:
            print('    - PRODUCT id: {} (profile: {}) (score: {}) '. format(
                result.product_id,
                result.product_features,
                result.score
            ))

    def _add_product(self, product_id: int, features: [int]):
        self.rec_sys.add_product(product_id=product_id, features=features)

    def _find_recommendations(self, product_ids: [int]):
        user_profile: [float] = \
            self.rec_sys.create_user_profile(product_ids=product_ids)
        recommendation_results: [RecommendationResult] = \
            self.rec_sys.get_recommendations(product_ids=product_ids)
        self._show_recommendation_results(
            user_profile=user_profile,
            recommendation_results=recommendation_results
        )

    def run(self):
        if self.parameters.option == 'ap':
            self._add_product(
                product_id=self.parameters.product_id,
                features=self.parameters.product_features
            )
        elif self.parameters.option == 'gr':
            self._find_recommendations(
                product_ids=self.parameters.product_ids
            )


if __name__ == '__main__':
    app: App = App(product_data=[
        (1, [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]),
        (2, [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]),
        (3, [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]),
        (4, [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]),
        (5, [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]),
        (6, [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]),
        (7, [0, 0, 0, 0, 0, 1, 0, 0, 0, 0])
    ])
    app.run()
