# iTechDay - Coding Dojo - Product Recommendation Application

## GOAL:
- Create a simple recommendation application
  based on content-based filtering approach
- MORE INFO:
  - https://www.upwork.com/resources/what-is-content-based-filtering (20/09/2021)
  - https://en.wikipedia.org/wiki/Recommender_system (20/09/2021)

## IN THIS PROCESS:
- Users and products should be represented by feature vectors;
- Feature vectors are compared to each other;
- Users may be compared to all products;
- Products with highest similarity score should be 
  returned as recommendation results for users.

## CONTENT-BASED FILTERING EXAMPLE:
- PRODUCT FEATURES:
  - GM: Electronic games
  - FB: Food & beverages
  - SC: Shoes & clothes 
- USER PROFILE:
  - Products of interest: [#1, #2, #5]
  - Profile (GM, FB, SC): [0.67, 0, 0.33]
- PRODUCT LIST:
  - PRODUCT #1: [1, 0, 0] -> SIMILARITY: 0.894427190999916
  - PRODUCT #2: [1, 0, 0] -> SIMILARITY: 0.894427190999916
  - PRODUCT #3: [1, 0, 0] -> SIMILARITY: 0.894427190999916
  - PRODUCT #4: [0, 0, 1] -> SIMILARITY: 0.447213595499958
  - PRODUCT #5: [0, 0, 1] -> SIMILARITY: 0.447213595499958
  - PRODUCT #6: [0, 1, 0] -> SIMILARITY: 0.0
- RECOMMENDATION RESULTS:
  - PRODUCT #3 - score: 0.894427190999916
  - PRODUCT #4 - score: 0.447213595499958

***

### PROJECT SETUP
- git clone https://gitlab.com/luizrevangelista/itechday_coding_dojo_recsys_application.git
- cd ./itechday_coding_dojo_recsys_application
- virtualenv venv -p python3
- source ./venv/bin/activate
- pip install -r requirements.txt

***

## CODING GUIDELINE

### PRODUCT FEATURES:
- BC - Books, magazines & comics
- FB - Food & beverages
- GM - Games
- HD - House & decoration
- JW - Jewes & watches
- MI - Musical instruments
- MM - Music & movies
- SC - Shoes & clothes
- SF - Sporting & fitness
- TH - Toys & hobbies

### RECOMMENDATION APPLICATION IN 4 STEPS - 120min
- Intro: 20min;
- Coding: 100min.

#### - STEP 1/4 - Add product ids and features (10min):
- Create product model instance (product id + product features);
- Add product model instance to a product dictionary having;
  product_id as key, allowing quick info access.

#### - STEP 2/4 - Create user profile (30min):
- Input parameter: 
  - User's product of interests (product ids).
- Sum all feature values of user's products, creating a new feature vector;
- Divide the new feature vector by the number of user's products;
- Return the modified feature vector as the user profile;
- Example:
  - Product features: [BC, FB, GM, HD, JW, MI, MM, SC, SF, TH]
  - Full product list:
    - Product #1: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    - Product #2: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    - Product #3: [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    - Product #4: [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  - User preferences - 3 products of interest:
      - Product #1: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
      - Product #2: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
      - Product #3: [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
  - Sum of all feature vectors: [0, 2, 1, 0, 0, 0, 0, 0, 0, 0]
  - Feature vectors divided by the number of products of interest: 
    - [0/3, 2/3, 1/3, 0/3, 0/3, 0/3, 0/3, 0/3, 0/3, 0/3]
  - User profile:
    - [0, 0.67, 0.33, 0, 0, 0, 0, 0, 0, 0]

#### - STEP 3/4 - Implement similarity method (20min):
- Input parameters: 
  - feature_1 - First feature vector;
  - feature_2 - Second feature vector;
- Calculate cosine similarity as:
  - inner(features_1, features_2) / ( norm(features_1) * norm(features_2) )
- Return similarity result;
- MORE INFO:
  - https://en.wikipedia.org/wiki/Cosine_similarity (21/09/2021).


#### - STEP 4/4 - Implement recommendation feature (40min):
- Input parameter: 
  - User's product of interests (product ids).
- Calculate the user profile based on his products of interest
- Compute the (cosine) similarity between the user profile and each product's features
- Return the product list inversely ordered by similarity values

***

## RUNNING THE APPLICATION:

### - OPTION "ADD PRODUCT"
- Syntax: 
  - python main.py -o=ap -pid=<product_id> -pf=<product_features>
- Feature examples - Game product:
  - [BC, FB, GM, HD, JW, MI, MM, SC, SF, TH]
  - [ 0,  0,  0,  1,  0,  0,  0,  0,  0,  0]
- Command line examples:
  - python main.py -o=ap -pid=1 -pf="[ 0,  0,  0,  1,  0,  0,  0,  0,  0,  0]"
  - python main.py -o=ap -pid=2 -pf="[ 0,  0,  0,  1,  0,  0,  0,  0,  0,  0]"
  - python main.py -o=ap -pid=3 -pf="[ 0,  0,  0,  1,  0,  0,  0,  0,  0,  0]"
  - python main.py -o=ap -pid=4 -pf="[ 0,  0,  0,  0,  0,  1,  0,  0,  0,  0]"
  - python main.py -o=ap -pid=5 -pf="[ 0,  0,  0,  0,  0,  1,  0,  0,  0,  0]"
  - python main.py -o=ap -pid=6 -pf="[ 0,  0,  0,  0,  0,  1,  0,  0,  0,  0]"
  - python main.py -o=ap -pid=7 -pf="[ 0,  0,  0,  0,  0,  1,  0,  0,  0,  0]"

### - OPTION "GET RECOMMENDATIONS"
- Syntax: 
  - python main.py -o=gr -pids=<product_ids>
- Command line example:
  - python main.py -o=gr -pids="[1, 2, 4]"
